#!/bin/python3

import math
import os
import random
import re
import sys


def evalPos(scores, temp):
    len1 = len(scores)
    global prev, pos, cond, i, iter1
    while i < len1 and scores[i] > temp:
        cond = True
        if i == 0:
            prev = scores[i]

        if prev != scores[i]:
            pos += 1
        prev = scores[i]
        i += 1

    if cond and iter1 < 1:
        iter1 += 1
        pos += 1
    return pos

def climbingLeaderboard(scores, alice):
    global prev, pos, cond, i, iter1
    iter1 = 0
    prev = 0
    i = 0
    pos = 1
    cond = False
    posArray = []
    alice.reverse()
    for score in alice:
        posArray.append(evalPos(scores, score))
    posArray.reverse()
    return posArray



if __name__ == '__main__':
    scores = [100, 90, 90, 80, 75, 60]
    alice = [50, 65, 77, 90, 102]
    hello = climbingLeaderboard(scores, alice)
    '''
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    scores_count = int(input())

    scores = list(map(int, input().rstrip().split()))

    alice_count = int(input())

    alice = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(scores, alice)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
    
    
    def evalPos(scores, temp):
    len1 = len(scores)
    i = 0
    prev = 0
    pos = 1
    cond = False
    while i < len1 and scores[i] > temp:
        cond = True
        if i == 0:
            prev = scores[i]

        if prev != scores[i]:
            pos += 1
        prev = scores[i]
        i += 1

    if cond:
        pos += 1
    print(pos)
    return pos
    
    
    
    def climbingLeaderboard(scores, alice):
    posArray = []

    for score in alice:
        posArray.append(evalPos(scores, score))

    return posArray
    
    
    
    i = 0
    leng = len(scores)
    alice.reverse()
    posArr = []
    pos = 1
    prev = 0
    j = 0
    check = alice[j]
    while i < leng:
        if i == 0:
            prev = scores[i]

        if prev != scores[i]:
            pos += 1

        if alice[j] >= scores[i]:
            posArr.append(pos)
            j += 1

        prev = scores[i]
        i += 1
    diff = len(alice) - len(posArr)
    while diff > 0:
        posArr.append(pos+1)
        diff -= 1

    posArr.reverse()
    for item in posArr:
        print(item)
    return posArr
    
    
    
    
    
    
    
    
    # Complete the climbingLeaderboard function below.
def climbingLeaderboard(scores, alice):
    len_s = len(alice)
    j = len_s - 1
    i = 0
    pos = 1
    posArr = []
    while i < len(scores):
        cond = False
        if i == 0:
            prev = scores[i]
        if alice[j] >= scores[i]:
            # print(alice[j], scores[i], prev)
            if prev != scores[i] and i != 0:
                pos += 1
                cond = True
            j -= 1
            posArr.append(pos)

        if not cond:
            if prev != scores[i]:
                pos += 1

        prev = scores[i]
        i += 1
    diff = len(alice) - len(posArr)
    while diff > 0:
        posArr.append(pos + 1)
        diff -= 1
    return posArr
    '''