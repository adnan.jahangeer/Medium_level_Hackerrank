#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the hurdleRace function below.
def hurdleRace(k, height):
    i = 0
    leng = len(height)
    max = height[i]
    i += 1
    while i < leng:
        if max < height[i]:
            max = height[i]
        i += 1
    if max < k:
        return 0
    return max - k

if __name__ == '__main__':

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    height = list(map(int, input().rstrip().split()))

    result = hurdleRace(k, height)

