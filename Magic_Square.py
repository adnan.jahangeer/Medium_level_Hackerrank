#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the formingMagicSquare function below.
def formingMagicSquare(s):
    cost = 0
    if s[1][1] != 5:
        print(s[1][1])
        diff = 5 - s[1][1]
        cost = abs(diff)
        s[1][1] = s[1][1] + diff
    print(cost)
    index = []
    check1 = [2, 4, 6, 8]
    check2 = [1, 3, 7, 9]
    i = 0
    j = 0
    while i < 3:
        j = 0
        while j < 3:
            if i == j == 1:
                pass
            else:
                if (i+j) % 2 == 0:
                    if s[i][j] in check1:
                        check1.remove(s[i][j])
                    else:
                        index.append([i, j])
                elif (i+j) % 2 != 0 :
                    if s[i][j] in check2:
                        check2.remove(s[i][j])
                    else:
                        index.append([i,j])
            j += 1
        i += 1

    for item in index:
        value = s[item[0]][item[1]]
        print("Value: ", value)
        if ((item[0] + item[1]) % 2 ) == 0:
            min = 0
            i = 0
            saveIndex = 0
            for min1 in check1:
                if min == 0:
                    min = min1
                    saveIndex = i
                else:
                    if s[item[0]][item[1]] - min > s[item[0]][item[1]] - min1:
                        min = min1
                        saveIndex = i
                i += 1
                # print("Min: ", (value - min))
            cost = cost + abs(value - min)
            check1.remove(check1[saveIndex])
        else:
            min = 0
            i = 0
            saveIndex = 0
            for min1 in check2:
                if min == 0:
                    min = min1
                else:
                    if s[item[0]][item[1]] - min > s[item[0]][item[1]] - min1:
                        min = min1
                        saveIndex = i
                i += 1
            # print("Min2: ", (value - min))
            cost = cost + abs(value - min)
            # print(saveIndex)
            check2.remove(check2[saveIndex])
    print(cost)
    return cost


if __name__ == '__main__':
    s = [[2, 2, 7], [8, 6, 4], [1, 2, 9]]
    formingMagicSquare(s)
    '''
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = []

    for _ in range(3):
        s.append(list(map(int, input().rstrip().split())))

    result = formingMagicSquare(s)

    fptr.write(str(result) + '\n')

    fptr.close()
    '''