#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the queensAttack function below.
def queensAttack(n, k, r_q, c_q, obstacles):
    r_q = n - r_q + 1

    print(r_q, c_q, n)
    cond11 = n - 1 - c_q
    cond12 = c_q
    cond21 = n - 1 - r_q
    cond22 = r_q
    total = (r_q % (n//2)) + (c_q % (n//2)) - 1
    cond31 = total - r_q
    cond32 = r_q

    cond41 = total - r_q
    cond42 = r_q
    print(cond42, cond41)
    total = cond11 + cond12 + cond21 + cond22 + cond31 + cond32 + cond41 + cond42
    print(total)
    for temp in obstacles:
        obstacle = temp
        obstacle[0] = n - temp[0] + 1
        obstacle[1] = temp[1]
        if obstacle[0] == r_q:
            if c_q > obstacle[1]:
                if cond11 > c_q - obstacle[1]-1:
                    cond11 = c_q - obstacle[1]-1
            else:
                if cond12 > obstacle[1] - c_q - 1:
                    cond12 = obstacle[1] - c_q - 1
        if obstacle[1] == c_q:
            if r_q > obstacle[1]:
                if cond21 > r_q - obstacle[0]-1:
                    cond21 = r_q - obstacle[0]-1
            else:
                if cond22 > obstacle[0] - r_q - 1:
                    cond22 = obstacle[0] - r_q - 1
        minus = r_q
        if minus > c_q:
            minus = c_q
        i = r_q - minus
        j = c_q - minus
        while i <= n and j <= n:
            if obstacle[0] == i and obstacle[1] == j:
                if r_q > obstacle[0]:
                    if cond31 > r_q - obstacle[0]:
                        cond31 = r_q - obstacle[0]
                else:
                    if cond32 > obstacle[0] - r_q:
                        cond32 = obstacle[0] - r_q
            i += 1
            j += 1
        i = r_q + c_q - 1
        j = 0
        while i > 0 and j <= n:
            if obstacle[0] == i and obstacle[1] == j:
                if r_q > obstacle[0]:
                    if cond41 > r_q - obstacle[0]:
                        cond41 = r_q - obstacle[0]
                else:
                    if cond42 > obstacle[0] - r_q:
                        cond42 = obstacle[0] - r_q
            i -= 1
            j += 1
    total = cond11 + cond12 + cond21 + cond22 + cond31 + cond32 + cond41 + cond42
    print(total)
    return total


if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    r_qC_q = input().split()

    r_q = int(r_qC_q[0])

    c_q = int(r_qC_q[1])

    obstacles = []

    for _ in range(k):
        obstacles.append(list(map(int, input().rstrip().split())))

    result = queensAttack(n, k, r_q, c_q, obstacles)

    # fptr.write(str(result) + '\n')

    # fptr.close()
